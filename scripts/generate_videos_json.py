# "sJFgo9H6zNo": {
#    "subset": "training",
#    "num_frames": 4164,
#    "url": "https://www.youtube.com/watch?v=sJFgo9H6zNo",
#    "duration": 139.042,
#    "resolution": "640x360", <= Not used...
#    "annotations": [
#      {
#        "segment": [
#          0.01,
#          123.42336739937599
#        ],
#        "label": "Fun sliding down"
#      }
#    ]
#}

import csv
import json
import random
from src.io_data import get_properties
if __name__ == '__main__':
    videosJson = {}
    with open('dataset/videos.csv') as csvfile:
        spamReader = csv.reader(csvfile)
        for row in spamReader:
            videoId = row[0]
            label = row[1]
            start = float(row[2])
            end = float(row[3])
            (num_frames, fps) = get_properties('data/videos/{}.mp4'.format(videoId))
            duration = num_frames / fps
            if videoId not in videosJson:
                subset = random.uniform(0, 1)
                videosJson[videoId] = {
                    "subset": 'training' if subset < 0.6 else if subset < 0.9 'validation' else 'testing',
                    "url": "https://www.youtube.com/watch?v={}".format(videoId),
                    "num_frames": num_frames,
                    "duration": duration,
                    "annotations": []
                }
            videosJson[videoId]["annotations"].append({
                "segment": [  start, end ],
                "label": label
            })
    with open('dataset/videos.json', 'w') as outfile:
        json.dump(videosJson, outfile)
